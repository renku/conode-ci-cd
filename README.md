# conode-ci-cd

Repository for CI/CD of SDSC's conode deployment

Note: since tiller is cluster-wide, need to run:
```bash
kubectl -n kube-system create rolebinding gitlab-conode --clusterrole=edit --serviceaccount=conode:conode-service-account
```
to make helm usable in deploy stages.
