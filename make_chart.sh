#!/usr/bin/env bash

set -ex

git clone https://github.com/SwissDataScienceCenter/cothority.git
cd cothority
git checkout helm-chart
cd ..
helm package cothority/helm-chart/conode
