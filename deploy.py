# -*- coding: utf-8 -*-

import sys
from subprocess import run, PIPE

import requests as r
from kubernetes import client, config

# Step 1: Get the current deployed tag
config.load_kube_config()
v1 = client.CoreV1Api()
pods = v1.list_namespaced_pod('conode')
conode_tag = None
for p in pods.items:
    if p.metadata.name.startswith('conode'):
        image = p.spec.containers[0].image
        repo, tag = image.split(sep=':', maxsplit=1)
        if 'conode' in repo:
            conode_tag = tag
            break

print('Detected conode tag: {}'.format(conode_tag), flush=True)


# Step 2: Get the most recent tag from Docker Hub
def gen_hub_tags():
    for obj in r.get('https://registry.hub.docker.com/v1/repositories/dedis/conode/tags').json():
        tag = obj.get('name', '')
        if tag.startswith('dev') and tag != 'dev-84-g3387e06':
            yield tag


hub_tag = sorted(gen_hub_tags())[-1]

print('Most recent conode tag: {}'.format(hub_tag), flush=True)

# Step 3: Redeploy if tags do not match
if conode_tag == hub_tag:
    sys.exit(1)
else:
    run(['bash', 'make_chart.sh']).check_returncode()

    proc1 = run(['helm', 'get', 'values', 'conode-1'], stdout=PIPE)
    proc1.check_returncode()
    with open('conode-1-values.yaml', 'wb') as f:
        f.write(proc1.stdout)
    run(['helm', 'upgrade','--install', 'conode-1', 'conode-0.1.0.tgz', '--namespace', 'conode',
         '-f', 'conode-1-values.yaml', '--set-string', 'image.tag={}'.format(hub_tag)]).check_returncode()

    proc2 = run(['helm', 'get', 'values', 'conode-2'], stdout=PIPE)
    proc2.check_returncode()
    with open('conode-2-values.yaml', 'wb') as f:
        f.write(proc2.stdout)
    run(['helm', 'upgrade','--install', 'conode-2', 'conode-0.1.0.tgz', '--namespace', 'conode',
         '-f', 'conode-2-values.yaml', '--set-string', 'image.tag={}'.format(hub_tag)]).check_returncode()
